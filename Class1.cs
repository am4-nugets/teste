﻿namespace Am4UpperCase.v2
{
    public class Class1
    {
        public static string ToUpperCase(string str)
        {
            return string.IsNullOrWhiteSpace(str) ? string.Empty : str.ToUpper();
        }

        public static string ToLowerCase(string str)
        {
            return string.IsNullOrWhiteSpace(str) ? string.Empty : str.ToLower();
        }

        public static int StringLenght(string str)
        {
            return string.IsNullOrWhiteSpace(str) ? 0 : str.Length;
        }
    }
}